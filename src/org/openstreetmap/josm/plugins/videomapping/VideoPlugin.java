package org.openstreetmap.josm.plugins.videomapping;

import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.plugins.videomapping.video.GPSVideoPlayerDialog;

/** This class is an entry point for the plugin.*/
public class VideoPlugin extends Plugin {
	private GPSVideoPlayerDialog gpsVideoPlayerDialog;

	public VideoPlugin(PluginInformation info) {
		super(info);
	}
	@Override
	public void mapFrameInitialized(MapFrame oldFrame, MapFrame newFrame) {
		if (oldFrame == null && newFrame != null) {
			gpsVideoPlayerDialog = new GPSVideoPlayerDialog();
			newFrame.addToggleDialog(gpsVideoPlayerDialog);
		}

	}

}
