/**
 * 
 */
package org.openstreetmap.josm.plugins.videomapping.video;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.data.gpx.WayPoint;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.MapView.LayerChangeListener;
import org.openstreetmap.josm.gui.SideButton;
import org.openstreetmap.josm.gui.dialogs.LayerListDialog;
import org.openstreetmap.josm.gui.dialogs.ToggleDialog;
import org.openstreetmap.josm.gui.layer.GpxLayer;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.plugins.videomapping.VideoPositionLayer;

import uk.co.caprica.vlcj.player.MediaPlayer;

/**
 * @author rsh
 * 
 */
public class GPSVideoPlayerDialog extends ToggleDialog implements
		LayerChangeListener {
	public final static String ICON_PREFIX = "videomapping";
	public final static String STR_ICON_OPEN = "open";
	public final static String STR_ICON_CREATE_LAYER = "link";
	public final static String STR_ICON_PLAY = "control_play";
	public final static String STR_ICON_PAUSE = "control_pause";
	public final static String STR_ICON_STOP = "control_stop";
	public final static String STR_ICON_LINK_GPS = "stopwatch_start";

	private final String PROP_MRU = "videomapping.mru";
	private final String PROP_AUTOCENTER = "videomapping.autocenter";
	private final String PROP_JUMPLENGTH = "videomapping.jumplength";

	private final GPSVideo gpsVideo;
	/** JOSM layer to show GPS data and video progress. */
	private VideoPositionLayer videoPositionLayer;

	/**
	 * Variable to determinate whether slider position only updated or need to
	 * fire events.
	 */
	private boolean isManualJump = true;

	/** Slider to display video progress. */
	private final JSlider slProgress;

	/** Listener for video player events. */
	private final GPSPlayerEventListener gpsPlayerEventListener;
	private final GPSPlayerJosmLayerMouseListener mapMouseListener;

	public GPSVideo getGpsVideo() {
		return gpsVideo;
	}

	// Actions
	private VideoOpenAction videoOpenAction;
	private VideoPlayAction videoPlayAction;
	private VideoStopAction videoStopAction;
	private VideoPauseAction videoPauseAction;
	private VideoLinkToGpsAction videoLinkToGpsAction;
	private VideoPopupVideoLayerAction videoPopupVideoLayerAction;

	private String mostRecentFolder;
	private Integer jumpLength;
	private boolean autoCenter;

	public JSlider getSlProgress() {
		return slProgress;
	}

	public GPSVideoPlayerDialog() {
		super(tr("Video mapping tool"), ICON_PREFIX,
				tr("Open videomapping tool"), null, 150, true);
		gpsVideo = new GPSVideo();
		slProgress = new JSlider(0, 100, 0);

		mapMouseListener = new GPSPlayerJosmLayerMouseListener(this);

		gpsPlayerEventListener = new GPSPlayerEventListener(this);
		this.setListeners();
		this.initUi();
		this.loadProperties();
		MapView.addLayerChangeListener(this);
	}

	private void setListeners() {
		slProgress.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				if (isManualJump) {
					MediaPlayer mediaPlayer = gpsVideo.getMediaPlayer();
					if (!mediaPlayer.isSeekable()) {
						return;
					}
					float positionValue = (float) slProgress.getValue() / 100.0f;
					// Avoid end of file freeze-up
					if (positionValue > 0.99f) {
						positionValue = 0.99f;
					}
					mediaPlayer.setPosition(positionValue);
				}
			}

		});

		Main.map.mapView.addMouseListener(this.mapMouseListener);
	}

	private void initUi() {
		JPanel pnMain = new JPanel(new BorderLayout());
		JPanel pnCanvas = new JPanel(new BorderLayout());
		pnCanvas.add(gpsVideo.getCanvas());
		pnMain.add(pnCanvas);
		createActions();
		Collection<SideButton> btns = new ArrayList<SideButton>();
		btns.add(new SideButton(videoOpenAction));
		btns.add(new SideButton(videoPopupVideoLayerAction));
		btns.add(new SideButton(videoPlayAction));
		btns.add(new SideButton(videoPauseAction));
		btns.add(new SideButton(videoStopAction));
		btns.add(new SideButton(videoLinkToGpsAction));
		pnMain.add(slProgress, BorderLayout.NORTH);
		createLayout(pnMain, false, btns);
	}

	private void createActions() {
		videoPlayAction = new VideoPlayAction(gpsVideo);
		videoOpenAction = new VideoOpenAction();
		videoPopupVideoLayerAction = new VideoPopupVideoLayerAction();
		videoPopupVideoLayerAction = new VideoPopupVideoLayerAction();
		videoStopAction = new VideoStopAction(gpsVideo);
		videoPauseAction = new VideoPauseAction(gpsVideo);
		videoLinkToGpsAction = new VideoLinkToGpsAction(gpsVideo);
	}

	private class VideoOpenAction extends JosmAction {


		public VideoOpenAction() {
			super(tr("Open"), STR_ICON_OPEN, "Open video file", null, true);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser(mostRecentFolder);
			fc.setSelectedFile(new File(mostRecentFolder));
			if (fc.showOpenDialog(Main.parent) != JFileChooser.CANCEL_OPTION) {
				mostRecentFolder = fc.getSelectedFile().getAbsolutePath();
				GPSVideoPlayerDialog.this.saveProperties();
				GPSVideoPlayerDialog.this.getGpsVideo().setMediaFile(
						fc.getSelectedFile());
				updateActionsStatuses();
			}

		}

		private void updateActionsStatuses() {
			videoLinkToGpsAction.setEnabled(null != videoPositionLayer
					&& null != gpsVideo.getMediaFile());
			videoPauseAction.setEnabled(true);
			videoPlayAction.setEnabled(true);
			videoStopAction.setEnabled(true);
		}
	}

	private class VideoPlayAction extends JosmAction {

		private GPSVideo video;

		public VideoPlayAction(GPSVideo video) {
			super(tr("Play"), STR_ICON_PLAY, "Play current video", null, true);
			setEnabled(false);
			this.video = video;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			video.getMediaPlayer().addMediaPlayerEventListener(
					gpsPlayerEventListener);
			video.play();
		}

	}

	private class VideoStopAction extends JosmAction {

		private GPSVideo video;

		public VideoStopAction(GPSVideo video) {
			super(tr("Stop"), STR_ICON_STOP, "Stop video", null, true);
			setEnabled(false);
			this.video = video;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (video.getMediaPlayer().isPlaying()) {
				video.getMediaPlayer().stop();
				video.getMediaPlayer().removeMediaPlayerEventListener(
						gpsPlayerEventListener);
				slProgress.setValue(0);
			}
		}

	}

	private class VideoPauseAction extends JosmAction {

		private GPSVideo video;

		public VideoPauseAction(GPSVideo video) {
			super(tr("Pause"), STR_ICON_PAUSE, "Pause video", null, true);
			setEnabled(false);
			this.video = video;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (video.getMediaPlayer().isPlaying()) {
				video.getMediaPlayer().pause();
				video.getMediaPlayer().removeMediaPlayerEventListener(
						gpsPlayerEventListener);
			}
		}

	}

	private class VideoLinkToGpsAction extends JosmAction {

		private GPSVideo video;

		public VideoLinkToGpsAction(GPSVideo video) {
			super(tr("Link"), STR_ICON_LINK_GPS, "Link video to GPS track.",
					null, true);
			setEnabled(false);
			this.video = video;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			videoPauseAction.actionPerformed(e);
			video.doSync(videoPositionLayer);
		}

	}

	private class VideoPopupVideoLayerAction extends JosmAction {

		public VideoPopupVideoLayerAction() {
			super(tr("Create video layer"), STR_ICON_CREATE_LAYER,
					"Create video layer from GPS.", null, true);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			boolean layerFound = false;
			List<Layer> layers = LayerListDialog.getInstance().getModel()
					.getLayers();
			JPopupMenu popupMenu = new JPopupMenu(
					tr("Select GPS layer as base for new video layer"));
			for (Layer layer : layers) {
				if (layer instanceof GpxLayer) {
					popupMenu.add(new VideoCreateVideoLayerAction(
							(GpxLayer) layer));
					layerFound = true;
				}
			}
			if (layerFound) {
				popupMenu.show((Component) e.getSource(), 0, 0);
			} else {
				JOptionPane.showMessageDialog(GPSVideoPlayerDialog.this,
						"No GPS layers found. Please load one.");
			}
		}

	}

	private class VideoCreateVideoLayerAction extends JosmAction {

		private GpxLayer layer;

		public VideoCreateVideoLayerAction(GpxLayer layer) {
			super(layer.getName(), STR_ICON_LINK_GPS, "", null, true);
			this.layer = layer;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			new VideoPositionLayer(layer);
		}

	}

	/** Method to update video timeline without triggering slider events. */
	public void updateProgress(int newProgress) {
		isManualJump = false;
		slProgress.setValue(newProgress);
		isManualJump = true;
	}

	/**
	 * Updates title for dialog to show current time progress.
	 * 
	 * @param newVideoDate
	 *            new date set for video.
	 */
	public void updateTitle(Date newVideoDate) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		WayPoint currentWayPoint = videoPositionLayer.getCurrentWayPoint();
		setTitle("Video:" + df.format(newVideoDate) + " point: "
				+ df.format(currentWayPoint.getTime()) + "("
				+ currentWayPoint.getCoor().getX() + ":"
				+ currentWayPoint.getCoor().getY() + ")");

	}

	/**
	 * @return the videoPositionLayer
	 */
	public VideoPositionLayer getVideoPositionLayer() {
		return videoPositionLayer;
	}

	private void loadProperties() {
		String temp;
		temp = Main.pref.get(PROP_AUTOCENTER);
		if ((temp != null) && (temp.length() != 0))
			autoCenter = Boolean.getBoolean(temp);
		temp = Main.pref.get(PROP_JUMPLENGTH);
		if ((temp != null) && (temp.length() != 0))
			jumpLength = Integer.valueOf(temp);
		temp = Main.pref.get(PROP_MRU);
		if ((temp != null) && (temp.length() != 0))
			mostRecentFolder = Main.pref.get(PROP_MRU);
	}

	private void saveProperties() {
		Main.pref.put(PROP_AUTOCENTER, autoCenter);
		Main.pref.put(PROP_JUMPLENGTH, jumpLength.toString());
		Main.pref.put(PROP_MRU, mostRecentFolder);
	}

	/**
	 * @return the mostRecentFolder
	 */
	public String getMostRecentFolder() {
		return mostRecentFolder;
	}

	/**
	 * @param mostRecentFolder
	 *            the mostRecentFolder to set
	 */
	public void setMostRecentFolder(String mostRecentFolder) {
		this.mostRecentFolder = mostRecentFolder;
	}

	/**
	 * @return the jumpLength
	 */
	public Integer getJumpLength() {
		return jumpLength;
	}

	/**
	 * @param jumpLength
	 *            the jumpLength to set
	 */
	public void setJumpLength(Integer jumpLength) {
		this.jumpLength = jumpLength;
	}

	/**
	 * @return the autoCenter
	 */
	public boolean isAutoCenter() {
		return autoCenter;
	}

	@Override
	public void activeLayerChange(Layer oldLayer, Layer newLayer) {
		if (newLayer instanceof VideoPositionLayer) {
			GPSVideoPlayerDialog.this.videoPositionLayer = (VideoPositionLayer) newLayer;
			videoLinkToGpsAction.setEnabled(null != videoPositionLayer
					&& null != gpsVideo.getMediaFile());
		}
	}

	@Override
	public void layerAdded(Layer newLayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void layerRemoved(Layer oldLayer) {
		if(oldLayer.equals(videoPositionLayer))
		{
			videoPauseAction.actionPerformed(null);
			videoPositionLayer.unload();
			videoPositionLayer = null;
			videoLinkToGpsAction.setEnabled(null != videoPositionLayer
					&& null != gpsVideo.getMediaFile());
			gpsVideo.doSync(null);
		}
	}
}
