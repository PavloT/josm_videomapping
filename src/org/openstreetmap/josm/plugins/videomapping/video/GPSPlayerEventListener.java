package org.openstreetmap.josm.plugins.videomapping.video;

import java.util.Date;

import javax.swing.SwingUtilities;

import org.openstreetmap.josm.plugins.videomapping.VideoPositionLayer;

import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;

public class GPSPlayerEventListener implements MediaPlayerEventListener {

	private GPSVideoPlayerDialog gpsVideoPlayerDialog;

	public GPSPlayerEventListener(GPSVideoPlayerDialog gpsVideoPlayerDialog) {
		this.gpsVideoPlayerDialog = gpsVideoPlayerDialog;

	}

	@Override
	public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media,
			String mrl) {
		// TODO Auto-generated method stub

	}

	@Override
	public void opening(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void buffering(MediaPlayer mediaPlayer, float newCache) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playing(MediaPlayer mediaPlayer) {

	}

	@Override
	public void paused(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopped(MediaPlayer mediaPlayer) {
	}

	@Override
	public void forward(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void backward(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finished(MediaPlayer mediaPlayer) {

	}

	@Override
	public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
		Runnable updateRunnable = new Runnable() {

			@Override
			public void run() {
				GPSVideo gpsVideo = gpsVideoPlayerDialog.getGpsVideo();
				int newProgress = Math.round(gpsVideo.getMediaPlayer()
						.getPosition() * 100);
				gpsVideoPlayerDialog.updateProgress(newProgress);

				if (gpsVideo.isSynced()) {
					long timeOffset = gpsVideo.getMediaPlayer().getTime()
							- gpsVideo.getSyncStartVideoTime();

					final Date newVideoDate = new Date(gpsVideo
							.getSyncStartWayPoint().getTime().getTime()
							+ timeOffset);
					VideoPositionLayer videoPositionLayer = gpsVideoPlayerDialog
							.getVideoPositionLayer();
					if (null != videoPositionLayer) {
						videoPositionLayer.jump(newVideoDate);
					}
					gpsVideoPlayerDialog.updateTitle(newVideoDate);
				}
			}
		};
		SwingUtilities.invokeLater(updateRunnable);
	}

	@Override
	public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
	}

	@Override
	public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pausableChanged(MediaPlayer mediaPlayer, int newSeekable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
		// TODO Auto-generated method stub

	}

	@Override
	public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
		// TODO Auto-generated method stub

	}

	@Override
	public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
		// TODO Auto-generated method stub

	}

	@Override
	public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaSubItemAdded(MediaPlayer mediaPlayer,
			libvlc_media_t subItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaFreed(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void newMedia(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
		// TODO Auto-generated method stub

	}

	@Override
	public void endOfSubItems(MediaPlayer mediaPlayer) {
		// TODO Auto-generated method stub

	}

}
