package org.openstreetmap.josm.plugins.videomapping.video;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.openstreetmap.josm.data.gpx.WayPoint;
import org.openstreetmap.josm.plugins.videomapping.VideoPositionLayer;

public class GPSPlayerJosmLayerMouseListener implements MouseListener {

	private GPSVideoPlayerDialog gpsVideoPlayerDialog;

	public GPSPlayerJosmLayerMouseListener(
			GPSVideoPlayerDialog gpsVideoPlayerDialog) {
		this.gpsVideoPlayerDialog = gpsVideoPlayerDialog;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			VideoPositionLayer videoPositionLayer = gpsVideoPlayerDialog
					.getVideoPositionLayer();
			if (null != videoPositionLayer) {
				WayPoint wp = videoPositionLayer.getNearestWayPoint(e.getPoint());
				if (wp != null) {
					if (gpsVideoPlayerDialog.getGpsVideo().isSynced()) {
						gpsVideoPlayerDialog.getGpsVideo().jumpTo(wp);
					}
					videoPositionLayer.setCurrentWayPoint(wp);
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
