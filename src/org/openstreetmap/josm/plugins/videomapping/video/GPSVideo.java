package org.openstreetmap.josm.plugins.videomapping.video;

import java.awt.Canvas;
import java.io.File;
import java.util.Date;

import org.openstreetmap.josm.data.gpx.WayPoint;
import org.openstreetmap.josm.plugins.videomapping.VideoPositionLayer;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;

/** This class is a bridge between mediaplayer and GPS data. */
public class GPSVideo {

	/** VLC options. See VLC docs for details */
	private final String[] libvlcArgs = { "" };
	/** VLC options. See VLC docs for details */
	private final String[] standardMediaOptions = { "" };

	/** File object for the video media. */
	private File mediaFile;
	/** Player used to render stream */
	private final EmbeddedMediaPlayer mediaPlayer;
	/** Canvas object used to render stream. */
	private final Canvas canvas;
	/** Internal VLCJ surface. */
	private final CanvasVideoSurface videoSurface;
	/** Factory to create mediaplayer object */
	private final MediaPlayerFactory mediaPlayerFactory;
	/** Start date set after sync. The date of the first sync-ed point. */
	private Date start;
	/** Last date set after sync. The date of the last sync-ed point. */
	private Date end;

	private VideoPositionLayer videoPositionLayer;

	/** Waypoint was set as first on sync procedure. */
	private WayPoint syncStartWayPoint;
	/** Offset from video start used in sync procedure. */
	private long syncStartVideoTime;

	/** Constructor. */
	public GPSVideo() {
		this.canvas = new Canvas();
		mediaPlayerFactory = new MediaPlayerFactory(libvlcArgs);
		mediaPlayer = mediaPlayerFactory.newEmbeddedMediaPlayer(null);
		mediaPlayer.setStandardMediaOptions(standardMediaOptions);
		videoSurface = mediaPlayerFactory.newVideoSurface(canvas);
		mediaPlayer.setVideoSurface(videoSurface);
	}

	/**
	 * Getter for waypoint from VideoPositionLayer which was selected as start
	 * point at sync procedure.
	 * 
	 * @return start waypoint set for this video, null if video is not sync.
	 */
	public WayPoint getSyncStartWayPoint() {
		return syncStartWayPoint;
	}

	/**
	 * Video time offset was used as media starting point on sync procedure.
	 * 
	 * @return media time offset in media time units.
	 */
	public long getSyncStartVideoTime() {
		return syncStartVideoTime;
	}

	/**
	 * Getter for embedded mediaplayer associated with this object.
	 * 
	 * @return reference to embedded mediaplayer.
	 */
	public EmbeddedMediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * Getter for current media file object
	 * 
	 * @return the file object for media file.
	 */
	public File getMediaFile() {
		return mediaFile;
	}

	/**
	 * Updates the media file associated with object. Also make internal player
	 * load this file and prepare to play.
	 * 
	 * @param mediaFile
	 *            the filename to set
	 */
	public void setMediaFile(File mediaFile) {
		mediaPlayer.prepareMedia(mediaFile.getAbsolutePath(),
				standardMediaOptions);
		// hack to make video loaded and paused immediately
		mediaPlayer.start();
		mediaPlayer.pause();
		this.mediaFile = mediaFile;
	}

	/** Plays current media file. */
	public void play()
	{
		if(!mediaPlayer.isPlayable())
		{
			mediaPlayer.prepareMedia(mediaFile.getAbsolutePath(),
					standardMediaOptions);
			
		}
		mediaPlayer.play();
	}
	/**
	 * Sync video stream with GPS layer.
	 * 
	 * @param layer
	 *            video positions layer to be sync with. If null video marked as
	 *            unsynced.
	 */
	public void doSync(VideoPositionLayer layer) {
		this.videoPositionLayer = layer;
		if (null != layer) {
			syncStartWayPoint = layer.getCurrentWayPoint();
			syncStartVideoTime = mediaPlayer.getTime();
			// syncStartVideoTime == -1 if media was stopped
			if (syncStartVideoTime == -1) {
				syncStartVideoTime = 0;
			}
			start = new Date(syncStartWayPoint.getTime().getTime());
			long videoLength = mediaPlayer.getLength();
			end = new Date(start.getTime() + (videoLength - syncStartVideoTime));
			markSyncedWayPoints();
		} else {
			syncStartWayPoint = null;
			start = null;
			end = null;
		}
	}

	/**
	 * Set video time according to given waypoint. If waypoint out of sync range
	 * - no changes applied.
	 * 
	 * @param wayPoint
	 *            waypoint to set.
	 */
	public void jumpTo(WayPoint wayPoint) {
		if (wayPoint.getTime().after(syncStartWayPoint.getTime())
				|| wayPoint.getTime().equals(syncStartWayPoint.getTime())) {
			long waypointOffset = wayPoint.getTime().getTime()
					- syncStartWayPoint.getTime().getTime();
			long videoLength = mediaPlayer.getLength();
			long newVideoTime = syncStartVideoTime + waypointOffset;
			if (newVideoTime <= videoLength) {
				mediaPlayer.setTime(newVideoTime);
			}
		}
	}

	/**
	 * Determinate whether current object is in sync with any layer.
	 * 
	 * @return true if object is in sync
	 */
	public boolean isSynced() {
		return syncStartWayPoint != null;
	}

	/** Release resources used by object before destroy. */
	public void unload() {
		if (null != mediaPlayer) {
			mediaPlayer.release();
		}
		if (null != mediaPlayerFactory) {
			mediaPlayerFactory.release();
		}
	}

	/** Update associated layer with media timeframe. */
	private void markSyncedWayPoints() {
		videoPositionLayer.resetSyncTimeframe(start, end);
		videoPositionLayer.setCurrentWayPoint(syncStartWayPoint);
	}

}
