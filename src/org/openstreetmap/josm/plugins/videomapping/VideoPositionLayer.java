package org.openstreetmap.josm.plugins.videomapping;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Action;
import javax.swing.Icon;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.gpx.GpxData;
import org.openstreetmap.josm.data.gpx.GpxTrack;
import org.openstreetmap.josm.data.gpx.GpxTrackSegment;
import org.openstreetmap.josm.data.gpx.WayPoint;
import org.openstreetmap.josm.data.osm.visitor.BoundingXYVisitor;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.dialogs.LayerListDialog;
import org.openstreetmap.josm.gui.dialogs.LayerListPopup;
import org.openstreetmap.josm.gui.layer.GpxLayer;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.tools.ImageProvider;

/**
 * This class intended to render GPS track on the separate layer and have
 * waypoints sync-ed to the video.
 */
public class VideoPositionLayer extends Layer {

	private static final String SYNC_KEY = "synced";

	/** Constant to identify layer in the JOSM layer stack. */
	private static final String VIDEO_LAYER_NAME_PREFIX = "video_r";

	/** List of the waypoints. */
	private List<WayPoint> gpsTrack;
	/** Current waypoint according to video position. */
	private WayPoint currentWayPoint;

	/** determinate whether should be centered on progress. */
	private boolean autoCenter;

	/**
	 * Constructor.
	 * 
	 * @param gpsLayer
	 *            layer which will be used to get GPS data.
	 */
	public VideoPositionLayer(GpxLayer gpsLayer) {
		super(VIDEO_LAYER_NAME_PREFIX+gpsLayer.getName());
		gpsTrack = importGPSLayer(gpsLayer.data);
		currentWayPoint = gpsTrack.get(0);
		Main.main.addLayer(this);

	}

	/** Make a copy of the gps waypoints and sort them according to the time. */
	private List<WayPoint> importGPSLayer(GpxData gps) {
		LinkedList<WayPoint> ls = new LinkedList<WayPoint>();
		for (GpxTrack trk : gps.tracks) {
			for (GpxTrackSegment segment : trk.getSegments()) {
				ls.addAll(segment.getWayPoints());
			}
		}
		Collections.sort(ls); // sort basing upon time
		return ls;
	}

	@Override
	public void paint(Graphics2D g, MapView map, Bounds bound) {
		paintGpsTrack(g, map);
		paintSyncedTrack(g, map);
		paintPositionIcon(g, map);
	}

	/** Paints all points from the track on this layer. */
	private void paintGpsTrack(Graphics2D g, MapView map) {
		g.setColor(Color.YELLOW); // TODO make this setting
		for (WayPoint n : gpsTrack) {
			Point p = map.getPoint(n.getEastNorth());
			g.drawOval(p.x - 2, p.y - 2, 4, 4);
		}
	}

	/** Paint only that part of the track which can be sync-ed with the video. */
	private void paintSyncedTrack(Graphics2D g, MapView map) {
		g.setColor(Color.GREEN);// TODO make this setting
		for (WayPoint n : gpsTrack) {
			if (n.attr.containsKey("synced")) {
				Point p = map.getPoint(n.getEastNorth());
				g.drawOval(p.x - 2, p.y - 2, 4, 4);
			}
		}

	}

	/**
	 * This method intended to reset sync attributes for all waypoints in the
	 * track.
	 * 
	 * @param start
	 *            starting date to be used for sync. could be null, in this case
	 *            all sync attributes are cleared.
	 * @param end
	 *            end date to be set. if start date !null and end date is null,
	 *            then sync flag set till end of the track.
	 */
	public void resetSyncTimeframe(Date start, Date end) {
		for (WayPoint n : gpsTrack) {
			n.attr.keySet().remove(SYNC_KEY);
			if (start != null && (start.equals(end) || start.before(end))) {
				if (n.getTime().equals(start) || n.getTime().after(start)) {
					if (end == null
							|| (n.getTime().before(end) || n.getTime().equals(
									end))) {
						// TODO make this Log4j
						DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						System.out.println("Waypoint time: "+df.format(n.getTime()));
						n.attr.put(SYNC_KEY, "test_id");
					}
				}
			}
		}
	}

	/** Paint a waypoint which is belong to the current position in the video. */
	private void paintPositionIcon(Graphics2D g, MapView map) {
		g.setColor(Color.RED);// TODO make this setting
		Point p = map.getPoint(currentWayPoint.getEastNorth());
		g.drawOval(p.x - 2, p.y - 2, 4, 4);
	}

	/** Getter for current layer waypoint. 
	 * @return current waypoint on this layer. */
	public WayPoint getCurrentWayPoint() {
		return currentWayPoint;
	}

	/**
	 * Returns waypoint by given time.
	 * 
	 * @param gpsTime
	 *            time for required waypoint.
	 * @return the waypoint for the given time, null if no waypoint for the
	 *         given moment.
	 */
	private WayPoint getWaypoint(Date gpsTime) {
		WayPoint result = null;
		if (gpsTime.after(currentWayPoint.getTime())) {
			WayPoint nextWayPoint = getNextWayPoint(currentWayPoint);
			while ((null != nextWayPoint)) {
				if (gpsTime.before(nextWayPoint.getTime())) {
					break;
				}
				result = nextWayPoint;
				nextWayPoint = getNextWayPoint(result);
			}
		}
		if (gpsTime.before(currentWayPoint.getTime())) {
			WayPoint prevWayPoint = getPrevWayPoint(currentWayPoint);
			while ((null != prevWayPoint)) {
				if (gpsTime.after(prevWayPoint.getTime())) {
					break;
				}
				result = prevWayPoint;
				prevWayPoint = getPrevWayPoint(result);
			}
		}

		return result;
	}

	/**
	 * Returns a waypoint next to given.
	 * 
	 * @param currentWayPoint
	 *            base waypoint. Must be in gpsTrack.
	 * @return next waypoint if any, null if no next waypoint exists.
	 */
	private WayPoint getNextWayPoint(WayPoint currentWayPoint) {
		WayPoint nextPoint = null;
		int currentIndex = gpsTrack.indexOf(currentWayPoint);
		if (currentIndex < gpsTrack.size() - 1) {
			nextPoint = gpsTrack.get(currentIndex + 1);
		}
		return nextPoint;
	}

	/**
	 * Returns a waypoint previous to given.
	 * 
	 * @param currentWayPoint
	 *            base waypoint. Must be in gpsTrack.
	 * @return next waypoint if any, null if no previous waypoint exists.
	 */
	private WayPoint getPrevWayPoint(WayPoint currentWayPoint) {
		WayPoint nextPoint = null;
		int currentIndex = gpsTrack.indexOf(currentWayPoint);
		if (currentIndex > 0) {
			nextPoint = gpsTrack.get(currentIndex - 1);
		}
		return nextPoint;
	}

	/**
	 * Setter for current waypoint.
	 * 
	 * @param wp
	 *            waypoint to be set. If null current waypoint is not changed,
	 *            but map repaint called.
	 * */
	public void setCurrentWayPoint(WayPoint wp) {
		if (null != wp) {
			currentWayPoint = wp;
		}
		Main.map.mapView.repaint();
		if (autoCenter) {
			Main.map.mapView.zoomTo(currentWayPoint.getCoor());
		}
	}

	/** Draw icon for given GPS time. */
	public void jump(Date GPSTime) {
		WayPoint newWaypoint = getWaypoint(GPSTime);
		setCurrentWayPoint(newWaypoint);
	}

	/** Finds the first waypoint that is nearby the given point */
	public WayPoint getNearestWayPoint(Point mouse) {
		final int MAX = 10;
		Point p;
		Rectangle rect = new Rectangle(mouse.x - MAX / 2, mouse.y - MAX / 2,
				MAX, MAX);
		// iterate through all possible notes
		for (WayPoint n : gpsTrack) {
			p = Main.map.mapView.getPoint(n.getEastNorth());
			if (rect.contains(p)) {
				return n;
			}

		}
		return null;

	}

	@Override
	public Icon getIcon() {
		return ImageProvider.get("video");
	}

	@Override
	public Action[] getMenuEntries() {
		return new Action[] {
				LayerListDialog.getInstance().createActivateLayerAction(this),
				LayerListDialog.getInstance().createShowHideLayerAction(),
				LayerListDialog.getInstance().createDeleteLayerAction(),
				SeparatorLayerAction.INSTANCE,
				new LayerListPopup.InfoAction(this) };
	}

	@Override
	public String getToolTipText() {
		return tr("Shows current position in the video");
	}

	@Override
	public boolean isMergable(Layer arg0) {
		return false;
	}

	public void unload() {
		Main.main.removeLayer(this);
	}

	@Override
	public Object getInfoComponent() {
		return null;
	}

	@Override
	public void mergeFrom(Layer arg0) {

	}

	@Override
	public void visitBoundingBox(BoundingXYVisitor arg0) {
		// TODO Auto-generated method stub

	}

	public void setAutocenter(boolean autoCenter) {
		this.autoCenter = autoCenter;
	}

	public boolean isAutocenter() {
		return autoCenter;
	}

}
